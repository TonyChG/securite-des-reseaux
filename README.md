
# TP - INTRODUCTION A LA SECURITE DES PROTOCOLES RESEAUX

[[_TOC_]]

Authors :

- Gaby Fulchic
- Antoine Chiny

## Machines

Un `Vagrantfile` est fourni dans le dépôt pour installer les machines.

```bash
vagrant up
```

| IPv4           | Nom      | Description                                                       |
|----------------|----------|-------------------------------------------------------------------|
| 192.168.254.2  | gateway  | Default gateway pour les deux autres machines                     |
| 192.168.254.5  | victim   | Joue le rôle de la victime dans un scénario d'ARP cache poisoning |
| 192.168.254.10 | attacker | Machine sur laquelle l'attaque sera lancé                         |

## Pré-requis

```bash
python3 -m pip install -r requirements.txt
```

## Découverte de l'outil

Utilisation basique de `scapy` dans un scénario simple.

Envoie d'une requête ARP, pour récupérer l'adresse MAC d'un cible.

Puis un simple ping ICMP sur la cible.

```bash
python3 scripts/getting_started.py 192.168.254.2
```

## Techniques avancées

### ARP cache poisonning

On vérifie d'abord que que la victime ping la gateway.

Sur le GIF suivant, on lance un ping depuis la victime vers la gateway

Sur le terminal de gauche.

```bash
vagrant ssh victim
ping 192.168.254.2
```

Sur les terminaux de droite.

En haut, on se connecte à la gateway puis on exécute une commande
`tcpdump` pour récupérer les ping de la victime.

```bash
vagrant ssh gateway
sudo tcpdump -i eth1 icmp
```

En bas, sur l'attaquant on exécute une commande `tcpdump` pour vérifier que
l'on ne reçoit aucun ping de la victime.

```bash
vagrant ssh attacker
sudo tcpdump -i eth1 icmp
```

La gateway reçoit bien tout les pings de la victime.

![before_arp_spoof](./images/before_arp_spoof.gif)

Pour le scénario d'ARP cache poisonning on va remplacer la MAC de la gateway par
celle de l'attaquant en effectuant des requêtes ARP.

Sur la machine qui attaque, on lance notre script qui va forger les paquets.

```bash
# Lancmement toutes les secondes du script d'ARP spoofing
watch -t sudo python3 scripts/arp_spoofing.py 192.168.254.10 192.168.254.2
```

On reprends la même configuration de terminaux que dans la première étape.

Les reçoit bien les ping de la victime sur l'attaquant.

La victime ne reçoit pas de réponse car on ne forward pas les paquets à la gateway.

Puis effectuer un autre ARP spoofing entre la gateway et l'attaquant.

```bash
# Lancmement toutes les secondes du script d'ARP spoofing
watch -t sudo python3 scripts/arp_spoofing.py 192.168.254.2 192.168.254.10
```

Si c'était le cas on serai dans une configuration `man-in-the-middle`.

![after_arp_spoof](./images/after_arp_spoof.gif)
