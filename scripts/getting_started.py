# coding: utf8
# vi: ft=python expandtab ts=4 sts=4 sw=4

import logging
import argparse

_log = logging.getLogger(__file__)

try:
    from scapy.all import *
    from rich.console import Console
    from rich.logging import RichHandler
except ImportError as exc:
    _log.fatal(
        "You need scapy to run this program "
        "run this command: sudo python3 -m pip install -r requirements.txt"
    )


def ping_arp4(dst: str) -> str:
    """Simple ICMPv4 ping using scapy

    Args:
        dst (str): The destination IPv4
    Return:
        str: Destination MAC address, None if there is no response
    """
    mac_address = None

    # Craft ARP frame : classic ARP request
    # Send ARP request and store ARP reply
    # It contains the MAC address of the target as the MAC source
    ans = sr1(ARP(op=1, pdst=dst), verbose=False, timeout=1)

    if ans is not None:
        # Extract MAC address from received ARP reply
        mac_address = ans[ARP].hwsrc
    if mac_address is not None:
        _log.info(ans.summary())
    else:
        _log.info(
            f"[bold red]{dst} did not reply to ARP request[/bold red]",
            extra={"markup": True}
        )
    return mac_address


def ping_icmp4(dst: str) -> bool:
    """Simple ICMPv4 ping using scapy

    Args:
        dst (str): The destination IPv4
    Return:
        bool: True if the destination reply to the ping
    """
    ans = sr1(IP(dst=dst)/ICMP(), verbose=False, timeout=1)
    dst_is_up = ans is not None

    if dst_is_up:
        _log.info(ans.summary())
    else:
        _log.info(
            f"[bold red]{dst} did not reply to ICMP ping[/bold red]",
            extra={"markup": True}
        )
    return dst_is_up


if __name__ == "__main__":
    FORMAT = "%(message)s"
    logging.basicConfig(
        level=logging.DEBUG, format=FORMAT, datefmt="[%X]", handlers=[RichHandler()]
    )

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "target", type=str,
        help=(
            "Launch a simple scenario of scans using scapy, it must be an IPv4 "
            "address example: 192.168.1.1"
        )
    )
    args = parser.parse_args()

    console = Console()

    target_is_up = False

    try:
        target_is_up = ping_icmp4(args.target)
        target_mac_address = ping_arp4(args.target)
    except PermissionError as exc:
        _log.fatal(exc)
